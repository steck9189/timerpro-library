package com.jonlab.timer_pro_library.model

import android.graphics.Paint
import java.util.ArrayList

/**
 * Created by Jonathan on 18/10/2021.
 */
class Section(var startClockPin: ClockPin, var endClockPin: ClockPin, var arcColor: Int) {

    internal var sweepAngle: Float = 0f
        private set
        get() {
            return if (endClockPin.angle < startClockPin.angle) {
                (360 - startClockPin.angle) + endClockPin.angle
            } else {
                endClockPin.angle - startClockPin.angle
            }
        }

    var isSelected = false


    internal var minAngle = -1f
        private set
    internal var maxAngle = -1f
        private set
    internal var isSectionOverMidnight = false
        private set

    fun findAndSetLimitOfSection(listOfSection: ArrayList<Section>) {
        minAngle = -1f  // reset dei valori
        maxAngle = -1f  // reset dei valori
        isSectionOverMidnight = false  // reset dei valori

        val tempList = listOfSection.filterNot { it == this }.sortedBy { it.startClockPin.angle }
        tempList.forEach {
            // trovo il min
            if (it.endClockPin.angle <= startClockPin.angle) {
                if (minAngle < 0) {
                    minAngle = it.endClockPin.angle
                } else {
                    if ((startClockPin.angle - minAngle) > (startClockPin.angle - it.endClockPin.angle)) {
                        minAngle = it.endClockPin.angle
                    }
                }
            }
            // trovo il max
            if (it.startClockPin.angle >= endClockPin.angle) {
                if (maxAngle < 0) {
                    maxAngle = it.startClockPin.angle
                } else {
                    if ((maxAngle - endClockPin.angle) > (it.startClockPin.angle - endClockPin.angle)) {
                        maxAngle = it.startClockPin.angle
                    }
                }
            }
        }

        if (maxAngle < 0) {
            maxAngle = tempList.first().startClockPin.angle
        }
        if (minAngle < 0) {
            minAngle = tempList.last().endClockPin.angle
        }

        isSectionOverMidnight = minAngle > maxAngle
    }

    internal val arcPaint = Paint()
        .apply {
            isAntiAlias = true
            color = arcColor
            style = Paint.Style.STROKE
//            strokeCap = Paint.Cap.ROUND
        }

    internal val selectorPaint = Paint().apply {
        isAntiAlias = true
        color = arcColor
        style = Paint.Style.FILL
    }
}