package com.jonlab.timer_pro_library.model

import androidx.annotation.IntRange

/**
 * Created by Jonathan on 18/10/2021.
 */
class ClockPin(@IntRange(from = 0, to = 24) var hour24: Int, @IntRange(from = 0, to = 59) var minute: Int) {

    var angle: Float=0f
        private set
        get() = 360 * ((hour24 * 60) + minute) / 1440f

    var isSelected = false

    fun setHourMinuteFromAngle(angle: Float) {
        val hm = 1440 * angle / 360
        hour24 = (hm / 60).toInt()
        minute = (hm - (hour24 * 60)).toInt()
    }

    fun getHumanTime(): String {
        return "${"%02d".format(hour24)}:${"%02d".format(minute)}"
    }
}