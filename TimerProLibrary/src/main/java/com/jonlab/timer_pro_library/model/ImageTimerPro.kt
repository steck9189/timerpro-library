package com.jonlab.timer_pro_library.model

import android.graphics.Rect
import androidx.annotation.DrawableRes
import com.jonlab.clocktest_1.customView.util.toPx

/**
 * Created by Jonathan on 26/10/2021.
 */
class ImageTimerPro(
    @DrawableRes val resImage: Int,
    val clockPin: ClockPin,
    widthDP: Int,
    heightDP: Int,
    val adjustRadius: Int = 0
) {
   internal val imageBound = Rect((-widthDP / 2).toPx, (-heightDP / 2).toPx, (widthDP / 2).toPx, (heightDP / 2).toPx)
}