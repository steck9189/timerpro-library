package com.jonlab.clocktest_1.customView.util

import android.content.res.Resources

/**
 * Created by Jonathan on 19/10/2021.
 */

// transform "this" from px to pd
val Int.toDp: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()

// transform "this" from dp to px
val Int.toPx: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()

// transform "this" from px to pd
val Float.toDp: Float
    get() = (this / Resources.getSystem().displayMetrics.density)

// transform "this" from dp to px
val Float.toPx: Float
    get() = (this * Resources.getSystem().displayMetrics.density)

val Float.toDegree: Float
    get() = (this * 180 / Math.PI).toFloat()

val Float.toRadian: Float
    get() = (this / 180 * Math.PI).toFloat()