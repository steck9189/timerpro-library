package com.jonlab.timer_pro_library

import android.content.Context
import android.graphics.*
import android.text.TextPaint
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import androidx.core.content.ContextCompat
import com.jonlab.clocktest_1.customView.util.toDegree
import com.jonlab.clocktest_1.customView.util.toPx
import com.jonlab.clocktest_1.customView.util.toRadian
import com.jonlab.timer_pro_library.model.ClockPin
import com.jonlab.timer_pro_library.model.ImageTimerPro
import com.jonlab.timer_pro_library.model.Section
import kotlin.math.*


/**
 * Created by Jonathan on 17/10/2021.
 */
class TimerPro @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : View(context, attrs, defStyleAttr) {

    private val TAG = this::class.java.simpleName
    private var drawDebugReference: Boolean = false

    // Attributi
    private var ringColor = Color.LTGRAY
    private var ringThicknessSize = 24f.toPx
    private var textPaintColor = Color.BLACK
    private var textPaintSize = 14f.toPx
    private var textPaintBold = true
    private var textPaintShow = true
    private var hoursIndicatorColor = Color.BLACK
    private var hoursIndicatorShow = true
    private var sectionSelectable = true

    // true quando sto muovendo il selettore
    private var sectionHasMoved = false

    // variabili per la gestione dell'overlap
    private var allowOverlap = true
    private var minimumSectionWidth = 360f * 30 / 1440

    // Listener
    private var listenerTimerPro: ITimeProListener? = null

    // Padding attorno al cerchio
    private var circlePadding = 10.toPx.toFloat()

    // Misura del canvan (è un quadrato, quindi è la misura del lato)
    private var size = -1

    // Centro del cerchio (utile perlopiù come contenitore)
    private lateinit var centro: PointF

    // Raggio del cerchio
    private var radius: Float = 0f

    // Rappresenta il rettangolo del selettore
    private lateinit var selector: RectF

    // Rappresenta l'area dove il canvas disegnerà gli archi
    private lateinit var areaArco: RectF

    // Lista delle sezioni di orari da rappresentare
    private val listOfSection = arrayListOf<Section>()

    // Lista di debug
    private val debugListOfSection = arrayListOf<Section>()

    // Paint per il debug
    private val debugPaint: Paint = Paint().apply {
        color = Color.BLACK
        isAntiAlias = true
        strokeWidth = 2f.toPx
        strokeCap = Paint.Cap.ROUND
        style = Paint.Style.STROKE
    }

    // Il TextPaint del testo
    private lateinit var textPaint: TextPaint

    // Il Paint del cerchio
    private lateinit var circlePaint: Paint

    // Paint delle stanghette orarie
    private lateinit var selectorPaint: Paint

    // variabile lista ImageTimerPro
    val listOfImageTimerPro = arrayListOf<ImageTimerPro>()

    // Debug variable utils ----------
    private var debugAreaSelectorStart: RectF? = null
    private var debugAreaSelectorEnd: RectF? = null
    private var debugAreaUserTouch: RectF? = null
    // -------------------------------

    init {
        attrs?.let { initAttrs(it, defStyleAttr) }
        initPaints()

        if (drawDebugReference) {
            debugListOfSection.add(Section(ClockPin(3, 0), ClockPin(6, 0), Color.parseColor("#98E9FF")).apply { isSelected = true })
            debugListOfSection.add(Section(ClockPin(9, 0), ClockPin(12, 0), Color.YELLOW))
            debugListOfSection.add(Section(ClockPin(15, 0), ClockPin(18, 0), Color.parseColor("#9DDF00")))
            debugListOfSection.add(Section(ClockPin(21, 0), ClockPin(24, 0), Color.parseColor("#98E9FF")))
            listOfSection.addAll(debugListOfSection)
        }
    }

    private fun initAttrs(attrs: AttributeSet, defStyle: Int) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.TimerPro, defStyle, 0)
        ringColor = a.getColor(R.styleable.TimerPro_timerPro_ring_color, ringColor)
        ringThicknessSize = a.getDimension(R.styleable.TimerPro_timerPro_ring_thickness_size, ringThicknessSize)
        textPaintColor = a.getColor(R.styleable.TimerPro_timerPro_text_paint_color, textPaintColor)
        textPaintSize = a.getDimension(R.styleable.TimerPro_timerPro_text_paint_size, textPaintSize)
        textPaintBold = a.getBoolean(R.styleable.TimerPro_timerPro_text_paint_bold, textPaintBold)
        textPaintShow = a.getBoolean(R.styleable.TimerPro_timerPro_text_show, textPaintShow)
        hoursIndicatorColor = a.getColor(R.styleable.TimerPro_timerPro_hours_indicator_color, hoursIndicatorColor)
        hoursIndicatorShow = a.getBoolean(R.styleable.TimerPro_timerPro_hours_indicator_show, hoursIndicatorShow)
        sectionSelectable = a.getBoolean(R.styleable.TimerPro_timerPro_section_selectable, sectionSelectable)
        a.recycle()
    }

    private fun initPaints() {
        textPaint = TextPaint().apply {
            color = textPaintColor
            isAntiAlias = true
            textSize = textPaintSize
            isFakeBoldText = textPaintBold
        }
        circlePaint = Paint().apply {
            color = ringColor
            isAntiAlias = true
            strokeWidth = ringThicknessSize
            style = Paint.Style.STROKE
        }
        selectorPaint = Paint().apply {
            isAntiAlias = true
            strokeCap = Paint.Cap.ROUND
            color = hoursIndicatorColor
            style = Paint.Style.FILL
        }
    }

    override fun onDraw(canvas: Canvas?) {
        Log.d(TAG, "onDraw(..) ")

        if (drawDebugReference) {
            canvas?.drawLine(size / 2f, 0f, size / 2f, size.toFloat(), debugPaint)
            canvas?.drawLine(0f, size / 2f, size.toFloat(), size / 2f, debugPaint)
            canvas?.drawRect(areaArco, debugPaint)
        }

        // Disegno il cerchio
        canvas?.drawCircle(centro.x, centro.y, radius, circlePaint)

        // Disegno inidicatore ore singole
        if (hoursIndicatorShow) {
            canvas?.let { drawOneHoursIndicators(it) }
        }

        // Disegno il testo di riferimento ogni 3 ore
        if (textPaintShow) {
            canvas?.let { drawTextEvery3Hrs(it) }
        }

        // Disegno le immagini interne
        canvas?.let { drawImagesTimer(it) }

        listOfSection.forEach { section ->
            // Disegno le sezioni di tempo
            canvas?.drawArc(areaArco, -90f + section.startClockPin.angle, section.sweepAngle, false, section.arcPaint)

            // Disegno i selettori
            if (section.isSelected && sectionSelectable) {
                canvas?.let { drawSelectors(it, section) }
            }
        }

        debugAreaSelectorStart?.let { canvas?.drawRect(it, debugPaint) }
        debugAreaSelectorEnd?.let { canvas?.drawRect(it, debugPaint) }
        debugAreaUserTouch?.let {
            canvas?.drawRect(it, debugPaint)
            canvas?.drawPoint(it.centerX(), it.centerY(), debugPaint)
        }

        super.onDraw(canvas)
    }

    private fun drawImagesTimer(canvas: Canvas) {
        listOfImageTimerPro.forEach {
            val radiusReference = radius - (circlePaint.strokeWidth * 3f) + it.adjustRadius
            canvas.save()
            val coord: PointF = calcolateCoordinates(radiusReference, it.clockPin.angle)
            canvas.translate(centro.x + coord.x, centro.y - coord.y)
            canvas.drawDrawable(
                context, it.resImage,
                it.imageBound.left, it.imageBound.top, it.imageBound.right, it.imageBound.bottom
            )
            if (drawDebugReference) {
                canvas.drawPoint(0f, 0f, debugPaint)
                canvas.drawRect(it.imageBound, debugPaint)
            }
            canvas.restore()
        }
    }

    private fun drawTextEvery3Hrs(canvas: Canvas) {
        val listOf1HrsMaster = floatArrayOf(0f, 45f, 90f, 135f, 180f, 225f, 270f, 315f)

        val radiusReference = radius - (circlePaint.strokeWidth * 2f)

        listOf1HrsMaster.forEach { angleH ->
            canvas.save()
            val coord: PointF = calcolateCoordinates(radiusReference, angleH)
            var mText = ""
            val bounds = Rect()

            when (angleH) {
                0f -> {
                    mText = "0"
                    textPaint.getTextBounds(mText, 0, mText.length, bounds)
                    coord.x = coord.x + (-bounds.width() / 2)
                    coord.y = coord.y + (-bounds.height() / 1.5f)
                }
                45f -> {
                    mText = "3"
                    textPaint.getTextBounds(mText, 0, mText.length, bounds)
                    coord.x = coord.x + (-bounds.width() / 1.5f)
                    coord.y = coord.y + (-bounds.height() / 2)
                }
                90f -> {
                    mText = "6"
                    textPaint.getTextBounds(mText, 0, mText.length, bounds)
                    coord.x = coord.x + (-bounds.width() / 2)
                    coord.y = coord.y + (-bounds.height() / 2)
                }
                135f -> {
                    mText = "9"
                    textPaint.getTextBounds(mText, 0, mText.length, bounds)
                    coord.x = coord.x + (-bounds.width() / 1.5f)
                    coord.y = coord.y + (-bounds.height() / 2)
                }
                180f -> {
                    mText = "12"
                    textPaint.getTextBounds(mText, 0, mText.length, bounds)
                    coord.x = coord.x + (-bounds.width() / 2)
                    coord.y = coord.y + (-bounds.height() / 4)
                }
                225f -> {
                    mText = "15"
                    textPaint.getTextBounds(mText, 0, mText.length, bounds)
                    coord.x = coord.x + (-bounds.width() / 4)
                    coord.y = coord.y + (-bounds.height() / 2)
                }
                270f -> {
                    mText = "18"
                    textPaint.getTextBounds(mText, 0, mText.length, bounds)
                    coord.x = coord.x + (-bounds.width() / 4)
                    coord.y = coord.y + (-bounds.height() / 2)
                }
                315f -> {
                    mText = "21"
                    textPaint.getTextBounds(mText, 0, mText.length, bounds)
                    coord.x = coord.x + (-bounds.width() / 4)
                    coord.y = coord.y + (-bounds.height() / 2)
                }
                else -> {
                }

            }

            canvas.translate(centro.x + coord.x, centro.y - coord.y)

            canvas.drawText(mText, 0f, 0f, textPaint)

            canvas.restore()
        }
    }

    private fun drawSelectors(canvas: Canvas, section: Section) {
        // selettore iniziale

        val selectorShadow = RectF(selector).apply {
            top += 3f.toPx
            bottom -= 3f.toPx
        }


        canvas.save()
        val coordStartSect: PointF = calcolateCoordinates(radius, section.startClockPin.angle)
        canvas.translate(centro.x + coordStartSect.x, centro.y - coordStartSect.y)
        canvas.rotate(section.startClockPin.angle)

        selectorShadow.apply { right += 5f.toPx }
        canvas.drawRoundRect(selectorShadow, selectorShadow.right, selectorShadow.right, Paint().apply {
            shader = RadialGradient(selectorShadow.top / 2, 0f, selectorShadow.right * 2, Color.BLACK, Color.TRANSPARENT, Shader.TileMode.CLAMP)
        })
        canvas.drawRoundRect(selector, selector.right, selector.right, section.selectorPaint)

        if (drawDebugReference) {
            canvas.drawPoint(0f, 0f, debugPaint)
        }

        canvas.restore()

        // selettore finale
        canvas.save()
        val coordEndSect: PointF = calcolateCoordinates(radius, section.endClockPin.angle)
        canvas.translate(centro.x + coordEndSect.x, centro.y - coordEndSect.y)
        canvas.rotate(section.endClockPin.angle)

        selectorShadow.apply {
            right -= 5f.toPx
            left -= 5f.toPx
        }
        canvas.drawRoundRect(selectorShadow, abs(selectorShadow.left), abs(selectorShadow.left), Paint().apply {
            shader =
                RadialGradient(selectorShadow.bottom / 2, 0f, abs(selectorShadow.left) * 2, Color.BLACK, Color.TRANSPARENT, Shader.TileMode.CLAMP)
        })
        canvas.drawRoundRect(selector, selector.right, selector.right, section.selectorPaint)

        if (drawDebugReference) {
            canvas.drawPoint(0f, 0f, debugPaint)
        }

        canvas.restore()
    }

    private fun drawOneHoursIndicators(canvas: Canvas) {
        val listOf1HrsMaster = floatArrayOf(0f, 45f, 90f, 135f, 180f, 225f, 270f, 315f)
        val listOf1Hrs = floatArrayOf(15f, 30f, 60f, 75f, 105f, 120f, 150f, 165f, 195f, 210f, 240f, 255f, 285f, 300f, 330f, 345f)

        val radiusReference = radius - (circlePaint.strokeWidth)

        for (angle in listOf1Hrs) {
            canvas.save()
            val coord: PointF = calcolateCoordinates(radiusReference, angle)
            canvas.translate(centro.x + coord.x, centro.y - coord.y)
            canvas.rotate(angle)

            val selector = RectF(-1f, 0f - circlePaint.strokeWidth / 4, 1f, 0f + circlePaint.strokeWidth / 4)

            canvas.drawRoundRect(selector, 10f, 10f, Paint(selectorPaint))

            canvas.restore()
        }

        for (angle in listOf1HrsMaster) {
            canvas.save()
            val coord: PointF = calcolateCoordinates(radiusReference, angle)
            canvas.translate(centro.x + coord.x, centro.y - coord.y)
            canvas.rotate(angle)

            val selector = RectF(-1f, 0f - circlePaint.strokeWidth / 4, 1f, circlePaint.strokeWidth / 2f)

            canvas.drawRoundRect(selector, 10f, 10f, Paint(selectorPaint))

            canvas.restore()
        }
    }


    override fun onTouchEvent(event: MotionEvent): Boolean {
//        return if (sectionSelectable) {
            return   when (event.action) {
                MotionEvent.ACTION_DOWN -> {

                    Log.d(TAG, "onTouchEvent(..) -> ACTION_DOWN")
                    PointF(event.x, event.y).also {

                        if (drawDebugReference) {
                            debugAreaSelectorStart = null
                            debugAreaSelectorEnd = null
                            debugAreaUserTouch = null
                        }

                        val angleTouched = calcolateAngle(centro, it)

                        listOfSection.map {
                            // pulisco i valori
                            it.apply {
                                isSelected = false
                                startClockPin.isSelected = false
                                endClockPin.isSelected = false
                            }
                        }.lastOrNull {
                            // cerco la sezione che corrisponde all'angolo cliccato
                            val lasco = 0

                            if (it.startClockPin.angle + it.sweepAngle > 360) {
                                if (angleTouched > it.startClockPin.angle) {
                                    angleTouched < 360
                                } else {
                                    angleTouched < it.endClockPin.angle
                                }
                            } else {
                                angleTouched > (it.startClockPin.angle - lasco) && angleTouched < (it.endClockPin.angle + lasco)
                            }
                        }?.apply {
                            isSelected = true

                            if (allowOverlap.not() && listOfSection.size > 1) {
                                // utility per NOT overlapping
                                this.findAndSetLimitOfSection(listOfSection)
                            }

                            // Calcolo l'area di dove l'utente ha cliccato
                            val areaTouchedUser = it.getArea(circlePaint.strokeWidth / 2).also {
                                if (drawDebugReference) {
                                    debugAreaUserTouch = it
                                }
                            }

                            // Coordinate del selettore Start
                            val coordSelettoreStart = calcolateCoordinates(radius, startClockPin.angle).apply {
                                x += centro.x
                                y = centro.y - y
                            }

                            // Calcolo area selezionabile del selettore Start
                            val areaSelettoreStartOnCircle = coordSelettoreStart.getArea(circlePaint.strokeWidth / 2).also {
                                if (drawDebugReference) {
                                    debugAreaSelectorStart = it
                                }
                            }

                            // Coordinate del selettore End
                            val coordSelettoreEnd = calcolateCoordinates(radius, endClockPin.angle).apply {
                                x += centro.x
                                y = centro.y - y
                            }

                            // Calcolo area selezionabile del selettore End
                            val areaSelettoreEndOnCircle = coordSelettoreEnd.getArea(circlePaint.strokeWidth / 2).also {
                                if (drawDebugReference) {
                                    debugAreaSelectorEnd = it
                                }
                            }

                            // Check intersect
                            if (areaSelettoreStartOnCircle.intersect(areaTouchedUser)) {
                                startClockPin.isSelected = true
                            } else if (areaSelettoreEndOnCircle.intersect(areaTouchedUser)) {
                                endClockPin.isSelected = true
                            }

                            listOfSection.sortBy { it.isSelected }

                            // Notifico al listener
                            listenerTimerPro?.onSectionSelected(this)
                        } ?: run { listenerTimerPro?.onSectionSelected(null) }
                    }

                    postInvalidate()

                    true
                }
                MotionEvent.ACTION_MOVE -> {
                    listOfSection.firstOrNull {
                        sectionSelectable && it.isSelected && (it.startClockPin.isSelected || it.endClockPin.isSelected)
                    }?.apply {

                        if (parent != null) {
                            parent.requestDisallowInterceptTouchEvent(true);
                        }

                        PointF(event.x, event.y).also {
                            var angolo = calcolateAngle(centro, it)
                            when {
                                startClockPin.isSelected -> {
                                    if (allowOverlap.not() && listOfSection.size > 1) {
                                        // logica di NON overlapping
                                        if (isSectionOverMidnight) {
                                            // se la sezione ha il startClockPin che può superare le 24h
                                            if (minAngle > endClockPin.angle) {
                                                // se il dito si trova nel cono d'ombra..
                                                if (angolo > endClockPin.angle - minimumSectionWidth && angolo < minAngle) {
                                                    // confronto le diff di angoli per capire quale angolo "finale" sia piu vicino
                                                    if (minAngle - angolo < angolo - endClockPin.angle) {
                                                        angolo = minAngle
                                                    } else {
                                                        angolo = endClockPin.angle - minimumSectionWidth
                                                    }
                                                }
                                            } else {
                                                // la sezione NON ha il startClockPin che può superare le 24h, quindi..
                                                if (angolo < minAngle) {
                                                    angolo = minAngle
                                                    // se il dito si trova nel cono d'ombra..
                                                } else if (angolo > endClockPin.angle - minimumSectionWidth && angolo < 360 || angolo < maxAngle) {
                                                    angolo = endClockPin.angle - minimumSectionWidth
                                                }
                                            }
                                        } else {
                                            if (angolo < minAngle) {
                                                angolo = minAngle
                                            }
                                            if (angolo > endClockPin.angle - minimumSectionWidth) {
                                                angolo = endClockPin.angle - minimumSectionWidth
                                            }
                                        }
                                    }
                                    startClockPin.setHourMinuteFromAngle(angolo)
                                }
                                endClockPin.isSelected -> {
                                    if (allowOverlap.not() && listOfSection.size > 1) {
                                        // logica di NON overlapping
                                        if (isSectionOverMidnight) {

                                            // se la sezione ha il endClockPin che può superare le 24h
                                            if (maxAngle < startClockPin.angle) {
                                                if (angolo > maxAngle && angolo < startClockPin.angle + minimumSectionWidth) {
                                                    angolo = if (angolo - startClockPin.angle + minimumSectionWidth < maxAngle - angolo) {
                                                        maxAngle
                                                    } else {
                                                        startClockPin.angle + minimumSectionWidth
                                                    }
                                                }
                                            } else {
                                                if (angolo > maxAngle) {
                                                    angolo = maxAngle
                                                } else if (angolo < startClockPin.angle + minimumSectionWidth) {
                                                    angolo = startClockPin.angle + minimumSectionWidth
                                                }
                                            }
                                        } else {
                                            if (angolo > maxAngle) {
                                                angolo = maxAngle
                                            }
                                            if (angolo < startClockPin.angle + minimumSectionWidth) {
                                                angolo = startClockPin.angle + minimumSectionWidth
                                            }
                                        }
                                    }
                                    endClockPin.setHourMinuteFromAngle(angolo)
                                }
                            }
                        }
                        sectionHasMoved = true
                        listenerTimerPro?.onSectionMove(this)
                        postInvalidate()
                    }

                    true
                }
                MotionEvent.ACTION_UP -> {
                    if (sectionHasMoved) {
                        val temp = listOfSection.map {
                            val tempStart = it.startClockPin.apply {
                                minute = minute.roundUp(5)
                                if (minute == 60) {
                                    minute = 0
                                    hour24++
                                }
                            }
                            val tempEnd = it.endClockPin.apply {
                                minute = minute.roundUp(5)
                                if (minute == 60) {
                                    minute = 0

                                }
                            }
                            it.apply {
                                startClockPin = tempStart
                                endClockPin = tempEnd
                            }.also {
                                if (it.isSelected) {
                                    listenerTimerPro?.onSectionMove(it)
                                }
                            }
                        }
                        listOfSection.clear()
                        listOfSection.addAll(temp)
                        listenerTimerPro?.onListOfSectionsUpdated(listOfSection)
                        sectionHasMoved = false
                    }
                    true
                }
                else -> {
                    super.onTouchEvent(event)
                }
            }
//        } else {
//            super.onTouchEvent(event)
//        }
    }

    private fun calcolateCoordinates(radius: Float, angleDegrees: Float): PointF {
        // https://www.mathepower.com/it/senocosenotangente.php
        val x = radius * sinOfDegrees(angleDegrees)
        val y = radius * cosOfDegrees(angleDegrees)
        return PointF(x, y)
    }

    private fun calcolateAngle(startPoint: PointF, endPoint: PointF): Float {
        val a = startPoint.y - endPoint.y
        val b = endPoint.x - startPoint.x
        val r = sqrt(a.pow(2) + b.pow(2))

        val fettaPositiva: Float
        val fettaNegativa: Float

        if (endPoint.x < startPoint.x && endPoint.y > centro.y) {
            fettaPositiva = 180f
            fettaNegativa = abs(asinToDegrees(b / r))
        } else if (endPoint.x < startPoint.x) {
            fettaPositiva = 270f
            fettaNegativa = asinToDegrees(a / r)
        } else {
            fettaPositiva = 90 - asinToDegrees(a / r)
            fettaNegativa = 0f
        }

        return (fettaPositiva + fettaNegativa).also {
            Log.d(TAG, "calcolateAngle: $it")
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        Log.d(TAG, "onMeasure() ")
        val widthSize = MeasureSpec.getSize(widthMeasureSpec)
        val heightSize = MeasureSpec.getSize(heightMeasureSpec)

        val width: Int =
            if (MeasureSpec.getMode(widthMeasureSpec) == MeasureSpec.UNSPECIFIED) {
                max(widthSize, heightSize)
            } else {
                widthSize
            }

        val height: Int =
            if (MeasureSpec.getMode(heightMeasureSpec) == MeasureSpec.UNSPECIFIED) {
                max(widthSize, heightSize)
            } else {
                heightSize
            }

        val newSize = min(width, height)
        if (newSize != size) {
            size = newSize
            centro = PointF(size.toFloat() / 2, size.toFloat() / 2)
            radius = centro.x - circlePadding - (circlePaint.strokeWidth / 2)
            areaArco = RectF(
                circlePadding + circlePaint.strokeWidth / 2,
                circlePadding + circlePaint.strokeWidth / 2,
                size - circlePadding - circlePaint.strokeWidth / 2,
                size - circlePadding - circlePaint.strokeWidth / 2
            )
            selector = RectF(
                0f - circlePaint.strokeWidth / 4,
                0f - circlePaint.strokeWidth / 3 - (circlePaint.strokeWidth * 0.4f),
                circlePaint.strokeWidth / 4,
                0f + circlePaint.strokeWidth / 3 + (circlePaint.strokeWidth * 0.4f)
            )
            invalidate()
        }
        val spec = MeasureSpec.makeMeasureSpec(newSize, MeasureSpec.EXACTLY)

        super.onMeasure(spec, spec)
    }

    interface ITimeProListener {
        fun onSectionSelected(section: Section?)
        fun onSectionMove(section: Section)
        fun onListOfSectionsUpdated(listOfSection: ArrayList<Section>)
    }

    fun setListenerTimer(iTimeProListener: ITimeProListener) {
        listenerTimerPro = iTimeProListener
    }

    fun setDebugMode(debugMode: Boolean, colorDebug: Int) {
        drawDebugReference = debugMode
        debugPaint.apply { color = colorDebug }
        invalidate()
    }

    /**
     * @param listOfSections rappresenta la lista delle sezioni da inserire nel grafico.
     */
    fun setListOfSections(listOfSections: ArrayList<Section>) {
        listOfSection.clear()
        listOfSection.addAll(listOfSections.map {
            // setto al Paint della sezione lo spessore dell'anello
            it.arcPaint.apply { strokeWidth = ringThicknessSize }
            it
        })
        invalidate()
    }

    /**
     * @param listOfImageTimerPro lista di immagini da inserire nel timer.
     */
    fun setListOfImage(listOfImageTimerPro: ArrayList<ImageTimerPro>) {
        this.listOfImageTimerPro.clear()
        this.listOfImageTimerPro.addAll(listOfImageTimerPro)
        invalidate()
    }

    /**
     * @param allowOverlap definisce se le sezioni possono sormontarsi.
     * @param minuteAsMinSectionWidth definisce in "Minuti" l'ampiezza minima che deve avere una [Section], questo parametro non è utilizzato se
     * [allowOverlap] è true.
     */
    fun setAllowOverlap(allowOverlap: Boolean, minuteAsMinSectionWidth: Int) {
        this.allowOverlap = allowOverlap
        this.minimumSectionWidth = 360f * minuteAsMinSectionWidth / 1440
    }

    fun setSectionSelectable(isSelectable: Boolean) {
        sectionSelectable = isSelectable
        postInvalidate()
    }

}

private fun PointF.getArea(distFromCenter: Float): RectF {
    return RectF(
        this.x - distFromCenter,
        this.y - distFromCenter,
        this.x + distFromCenter,
        this.y + distFromCenter
    )
}

/**
 * ATTENZIONE: Passando come [idResource] un'immagine in formato png o jpg farà crashare la preview ma nello smartphone verrà disegnata.
 *
 * @param context Context
 * @param idResource id del drawable svg, png o jpg
 * @param leftBound_Px da centro dell'immagine, rappresenta in pixel quanto dista il bordo sinistro (valore negativo)
 * @param rightBound_Px da centro dell'immagine, rappresenta in pixel quanto dista il bordo destro
 * @param topBound_Px da centro dell'immagine, rappresenta in pixel quanto dista il bordo alto (valore negativo)
 * @param bottomBound_Px da centro dell'immagine, rappresenta in pixel quanto dista il bordo basso
 *
 */
private fun Canvas.drawDrawable(context: Context, idResource: Int, leftBound_Px: Int, topBound_Px: Int, rightBound_Px: Int, bottomBound_Px: Int) {
    ContextCompat.getDrawable(context, idResource)
        ?.apply {
            setBounds(leftBound_Px, topBound_Px, rightBound_Px, bottomBound_Px)
        }?.also {
            it.draw(this)
        }
}

/**
 * Arrotonda al multiplo di [nearestMultipleOf] più vicino
 */
private fun Int.roundUp(nearestMultipleOf: Int): Int {
    return (round((this / nearestMultipleOf.toFloat())) * nearestMultipleOf).toInt()
}

/**
 * Coseno di un angolo rappresentato in gradi decimali (NON radianti)
 */
private fun cosOfDegrees(angleDegrees: Float): Float {
    return cos(angleDegrees.toRadian)
}

/**
 * Seno di un angolo rappresentato in gradi decimali (NON radianti)
 */
private fun sinOfDegrees(angleDegrees: Float): Float {
    return sin(angleDegrees.toRadian)
}

/**
 * @return l'arcseno di una misura espresso in gradi decimali (NON radianti)
 */
private fun asinToDegrees(value: Float): Float {
    return asin(value).toDegree
}