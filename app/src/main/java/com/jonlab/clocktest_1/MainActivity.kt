package com.jonlab.clocktest_1

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import com.google.gson.Gson
import com.jonlab.clocktest_1.databinding.ActivityMainBinding
import com.jonlab.timer_pro_library.TimerPro
import com.jonlab.timer_pro_library.model.ClockPin
import com.jonlab.timer_pro_library.model.ImageTimerPro
import com.jonlab.timer_pro_library.model.Section

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var allowOverlap = false
    private var minuteAsMinSectionWidth = 60

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.editTextNumber.apply {
            setText(minuteAsMinSectionWidth.toString())
            addTextChangedListener {
                minuteAsMinSectionWidth = it?.toString()?.toIntOrNull() ?: 60
                binding.timePro.setAllowOverlap(allowOverlap, minuteAsMinSectionWidth)
            }
        }
        binding.switchOverlap.apply {
            isChecked = allowOverlap
            setOnCheckedChangeListener { _, isChecked ->
                allowOverlap = isChecked
                binding.timePro.setAllowOverlap(allowOverlap, minuteAsMinSectionWidth)
            }
        }

        binding.timePro.setDebugMode(false, Color.WHITE)

        val listOfSection = arrayListOf<Section>()
        listOfSection.add(Section(ClockPin(3, 0), ClockPin(7, 0), Color.parseColor("#98E9FF")))
        listOfSection.add(Section(ClockPin(15, 0), ClockPin(18, 30), Color.parseColor("#9DDF00")))
        listOfSection.add(Section(ClockPin(21, 0), ClockPin(2, 0), Color.RED))
        listOfSection.add(Section(ClockPin(9, 23), ClockPin(11, 10), Color.YELLOW))
//        listOfSection.add(Section(ClockPin(0, 0), ClockPin(24, 0), Color.RED))


        val sole = ImageTimerPro(R.drawable.ic_sun, ClockPin(5, 30), 25, 25)
        val luna = ImageTimerPro(R.drawable.ic_moon, ClockPin(19, 20), 30, 30)
        val freccia = ImageTimerPro(R.drawable.ic_up_arrow, ClockPin(18, 0), 20, 25)


        binding.timePro.setAllowOverlap(
            binding.switchOverlap.isChecked,
            binding.editTextNumber.text.toString().toIntOrNull() ?: minuteAsMinSectionWidth
        )
        binding.timePro.setListOfImage(arrayListOf(sole, luna, freccia))
        binding.timePro.setListOfSections(listOfSection)
        binding.timePro.setListenerTimer(object : TimerPro.ITimeProListener {
            @SuppressLint("SetTextI18n")
            override fun onSectionSelected(section: Section?) {
                binding.tvStart.text = section?.startClockPin?.getHumanTime().orEmpty().ifEmpty { "--:--" }
                binding.tvEnd.text = section?.endClockPin?.getHumanTime().orEmpty().ifEmpty { "--:--" }
                binding.tvDebug.text = Gson().toJson(section)
            }

            override fun onSectionMove(section: Section) {
                binding.tvStart.text = section.startClockPin.getHumanTime()
                binding.tvEnd.text = section.endClockPin.getHumanTime()
            }

            override fun onListOfSectionsUpdated(listOfSection: ArrayList<Section>) {
                Log.d("dcjfrvjrnfvj", "onListOfSectionsUpdated: ${Gson().toJson(listOfSection)}")
            }
        })
    }

}